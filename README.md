# IMPORTANT NOTE
**We are currently working on an improved implementation of the LPV-SUBNET code in the DeepSI *Lite* toolbox. See [DeepSI Lite](https://github.com/GerbenBeintema/deepSI_lite.git) for more information. The current implementation of LPV-SUBNET can be found [here](https://github.com/GerbenBeintema/deepSI_lite/blob/main/deepSI_lite/SUBNET.py#L174) -- see `class SUBNET_LPV(Custom_SUBNET)`.**


# Software associated with LPV-SUBNET paper
LPV-SUBNET code and data, released for the 2022 CDC paper "Deep-Learning-Based Identification of LPV Models for Nonlinear Systems"

## Dependencies
In order to run the included code, one will need:

- The [LPVcore](https://www.lpvcore.net) toolbox for MATLAB
- The Python [deepSI](https://github.com/GerbenBeintema/deepSI/) toolbox

The code is tested for Python 3.9.7 and MATLAB 2023b.

#### Update (March, 2024): Due to a modification in LPVcore, the identification code has been updated. Make sure the most recent version of LPV core is used.

## Additional information
The data is generated with a simulator of the CMG, which can be found [here](https://gitlab.com/TomBloemers/gyroscope). 

If you want to quickly link to this repository you can use: [tinyurl.com/LPV-SUBNET-data](tinyurl.com/LPV-SUBNET-data).

If you want to quickly link to the paper you can use: [tinyurl.com/LPV-SUBNET-paper](tinyurl.com/LPV-SUBNET-paper).

Please cite the paper as:

> C. Verhoek, G. I. Beintema, S. Haesaert, M. Schoukens and R. Tóth. "Deep-Learning-Based Identification of LPV Models for Nonlinear Systems." In *Proc. of the 61st IEEE Conference on Decision and Control (CDC)*, 2022, pp. 3274-3280.
